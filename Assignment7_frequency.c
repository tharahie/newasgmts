#include <stdio.h>
#define MAX	50

//This programme will find the frequency of a character in a string

int main()
{
	char str[MAX]={0};
	char ch;
	int a,count;

	printf("Enter the string:");
	scanf("%[^\n]s",str);
	
	getchar(); 
	
	
	printf("Enter a character to find its frequency:");
	ch=getchar();
	
	
	count=0;
	for(a=0; str[a]!='\0'; a++)
	{
		if(str[a]==ch)
			count++;
	}
	printf("Frequency of %c is %d\n",ch,count);
	
	return 0;	
}

