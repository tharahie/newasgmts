#include <stdio.h>
//This programme will show the relationship between profit and ticket price in order to determine the price at which highest profit can be made

int noattend(int);
int revenue(int);
int cost(int);
int profit(int);

int noattend(int p)
{
return 120-(p-15)/5*20;
}

int revenue(int p)
{
return noattend(p)*p;
}

int cost(int p)
{
return 500 + noattend(p)*3;
}

int profit(int p)
{
return revenue(p) - cost(p);
}

int main()
{
printf("Profits = %d %d %d %d %d\n",profit(15),profit(20),profit(25),profit(30),profit(35));
return 0;
}


