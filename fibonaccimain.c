#include <stdio.h>
//This programme prints fibonacci sequence up to n

int fibonacci(int);

int main()
{
    int n, i = 0, a;
    
    printf("Enter the term n to display the fibonacci sequence:\n");
    scanf("%d", &n);
    
    printf("Fibonacci series:\n");
    for(a=1; a<=n; a++)
    {
        printf("%d\n", fibonacci(i));
        i++;
    }
    
    return 0;
}