#include <stdio.h>
//This programme prints a number triangle

void pattern(int n);
 
int main( )
{
    int n;
    printf("Enter the number of lines you want to print:");
    scanf("%d",&n);
    pattern(n);
    return 0;
}
 
void pattern(int n)
{
    int i, j;
    if(n==0)
    return;
    else
    {
    pattern(n-1);
    for(i=n; i>=1; i--)
               
    {
    printf("%d ",i);
    }
    
    printf("\n");
               
    }
        
}
