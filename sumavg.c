#include <stdio.h>
//This programme will calculate sum and average of 3 integers

int main()
{
    int a=0, b=0, c=0, sum=0, avg=0;
    printf("Enter first integer:\n");
    scanf("%d",&a);
    printf("Enter second integer:\n");
    scanf("%d",&b);
    printf("Enter third integer:\n");
    scanf("%d",&c);
    sum=a+b+c;
    printf("Sum=%d\n",sum);
    avg=sum/3;
    printf("Average=%d\n",avg);
    return 0;
}

