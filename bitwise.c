#include <stdio.h>
//This programme will demonstrate bitwise operators

int main()
{
    unsigned int A=10,B=15,output;
    
    output=A&B;
    printf("Output %d & %d = %d\n",A, B, output);
    
    output=A^B;
    printf("Output %d ^ %d = %d\n",A, B, output);
    
    output=~A;
    printf("Output ~%d = %d\n",A, output);
    
    output=A<<3;
    printf("Output %d<<3 = %d\n",A, output);
    
    output=B>>3;
    printf("Output %d>>3 = %d\n",B, output);
    
   
    return 0;
}
    
    
