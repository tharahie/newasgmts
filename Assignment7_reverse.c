#include <stdio.h>
#include <string.h>
 
 //This programme will reverse a sentence
 
int main()
{
  	char str[100];
  	int a, b, len, indexBegin, indexEnd;
 
  	printf("Enter a sentence:");
  	scanf("%[^\n]s",str);
  	
  	len = strlen(str);
  	indexEnd = len - 1;
  	   	
  	for(a = len - 1; a >= 0; a--)
	{
		if(str[a] == ' ' || a == 0)
		{
			if(a == 0)
			{
				indexBegin = 0;
			}
			else
			{
				indexBegin = a + 1;
			}
			for(b = indexBegin; b <= indexEnd; b++)
			{
				printf("%c", str[b]);
			}
			indexEnd = a - 1;
			printf(" ");				
		} 
	}
	
  	return 0;
}

