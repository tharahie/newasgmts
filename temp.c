#include <stdio.h>
//This programme will convert temperature given in Celsius to Fahrenheit

float main()
{
    float c=0,f=0;
    printf("Enter Celsius temperature:\n");
    scanf("%f",&c);
    f=(c*9/5)+32;
    printf("Fahrenheit equivalent=%.2f\n",f);
    return 0;
}