#include <stdio.h>
//This programme will store and display the information of students

struct student 
{
    char firstname[80];
    char subject[20];
    float marks;
} 
s[10];

int main() 
{
    int i;
    printf("Please enter the information of students:\n");
    for (i = 0; i < 5; ++i)
    {
        printf("Enter first name of student: ");
        scanf("%s", s[i].firstname);
         printf("Enter subject: ");
        scanf("%s", s[i].subject);
        printf("Enter marks of student: ");
        scanf("%f", &s[i].marks);
    }
    printf("\n");
    printf("The information of students is displayed below:\n\n");
    for (i = 0; i < 5; ++i)
    {
        printf("First name of student: ");
        puts(s[i].firstname);
        printf("Subject: ");
        puts(s[i].subject);
        printf("Marks of student: %.2f", s[i].marks);
        printf("\n");
    }
    return 0;
}
