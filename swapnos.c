#include <stdio.h>
// This programme will swap two numbers

float main()
{
    float x=0,y=0;
    printf("Enter first number:\n");
    scanf("%f",&x);
    printf("Enter second number:\n");
    scanf("%f",&y);
    printf("Before swapping\nx=%.2f y=%.2f",x,y);
    x=x*y;
    y=x/y;
    x=x/y;
    printf("After swapping\nx = %.2f y = %.2f",x,y);
    return 0;
}