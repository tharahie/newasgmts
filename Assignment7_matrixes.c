#include <stdio.h>

#define SIZE 2 

int main()
{
    int x[SIZE][SIZE];  
    int y[SIZE][SIZE]; 
    int z[SIZE][SIZE]; 
    
    int m, row, col, sum;
   
    printf("Please enter the elements of Matrix 1 of size %dx%d: \n", SIZE, SIZE);
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            scanf("%d", &x[row][col]);
        }
    }
   
    printf("Please enter the elements of Matrix 2 of size %dx%d: \n", SIZE, SIZE);
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            scanf("%d", &y[row][col]);
        }
    }
   
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            z[row][col] = x[row][col] + y[row][col];
        }
    }
   
    printf("The result after multiplying Matrix 1 + Matrix 2\n");
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            printf("%d ", z[row][col]);
        }
        printf("\n");
    }
  
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            sum = 0;
            
            for(m=0; m<SIZE; m++)
            {
                sum += x[row][m] * y[m][col];
            }

            z[row][col] = sum;
        }
    }
 
    printf("The result after multiplying Matrix 1 x Matrix 2:\n");
    for(row=0; row<SIZE; row++)
    {
        for(col=0; col<SIZE; col++)
        {
            printf("%d ", z[row][col]);
        }
        printf("\n");
    }
    
    return 0;
}
